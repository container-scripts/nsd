#!/bin/bash

# make sure that curl is installed
hash curl 2>/dev/null || apt install --yes curl

get_public_ip() {
    local services='ifconfig.co ifconfig.me icanhazip.com'
    local pattern='^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$'
    local ip
    for service in $services; do
        ip=$(curl -s $service)
        [[ $ip =~ $pattern ]] && echo $ip && return
    done
    return 1
}

# copy example zone db
mkdir -p zones/
cp $APPS/nsd/misc/example.org.db zones/

# fix the ip on the example zone file
sed -i zones/example.org.db \
    -e "s/127\.0\.0\.1/$(get_public_ip)/"

# create nsd config file
mkdir -p config
cat <<EOF > config/nsd.conf
server:
    do-ip6: no
    zonesdir: /host/zones
    verbosity: 2

pattern:
    name: axfr-servers
    include: /host/config/notify.conf
    include: /host/config/provide-xfr.conf

include: /host/zones/*.zone
EOF

#cp $APPS/nsd/misc/googlehosted config/
#cp $APPS/nsd/misc/google.mx config/
