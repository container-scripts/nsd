APP=nsd

IMAGE=nsd
CONTAINER=nsd

### Check these:
### https://www.buddyns.com
### https://puck.nether.net/
### http://networking.ringofsaturn.com/Unix/freednsservers.php
SECONDARY_NS="
    uz5x36jqv06q5yulzwcblfzcrk1b479xdttdm1nrgfglzs57bmctl8.free.ns.buddyns.com
    uz56xw8h7fw656bpfv84pctjbl9rbzbqrw4rpzdhtvzyltpjdmx0zq.free.ns.buddyns.com
    uz5x6wcwzfbjs8fkmkuchydn9339lf7xbxdmnp038cmyjlgg9sprr2.free.ns.buddyns.com
    uz5qfm8n244kn4qz8mh437w9kzvpudduwyldp5361v9n0vh8sx5ucu.free.ns.buddyns.com
    uz5dkwpjfvfwb9rh1qj93mtup0gw65s6j7vqqumch0r9gzlu8qxx39.free.ns.buddyns.com
    uz53c7fwlc89h7jrbxcsnxfwjw8k6jtg56l4yvhm6p2xf496c0xl40.free.ns.buddyns.com
"
AXFR_SERVERS="
    108.61.224.67
    116.203.6.3
    107.191.99.111
    185.22.172.112
    103.6.87.125
    192.184.93.99
    119.252.20.56
    107.181.178.180
    185.34.136.178
    185.136.176.247
    45.77.29.133
    116.203.0.64
    167.88.161.228
    199.195.249.208
    104.244.78.122
"

### SMTP server for sending notifications. You can build an SMTP server
### as described here:
### https://gitlab.com/container-scripts/postfix/blob/master/INSTALL.md
### Comment out if you don't have a SMTP server and want to use
### a gmail account (as described below).
#SMTP_SERVER=smtp.example.org
#SMTP_DOMAIN=example.org

### Gmail account for notifications. This will be used by ssmtp.
### You need to create an application specific password for your account:
### https://www.lifewire.com/get-a-password-to-access-gmail-by-pop-imap-2-1171882
#GMAIL_ADDRESS=
#GMAIL_PASSWD=
