cmd_config_help() {
    cat <<_EOF
    config
        Run configuration scripts inside the container.

_EOF
}

cmd_config() {
    cs inject ubuntu-fixes.sh
    cs inject ssmtp.sh
    cs inject logwatch.sh

    _config
    
    cs inject setup.sh
}

_config() {
    # create 'notify' and 'provide-xfr' config files
    echo "notify: 127.0.0.1 NOKEY" > config/notify.conf
    echo "provide-xfr: 127.0.0.1 NOKEY" > config/provide-xfr.conf
    for server_ip in $AXFR_SERVERS; do
        echo "notify: $server_ip NOKEY" >> config/notify.conf
        echo "provide-xfr: $server_ip NOKEY" >> config/provide-xfr.conf
    done

    # create 'secondary.ns' include file
    > config/secondary.ns
    for ns in $SECONDARY_NS; do
        echo "@  IN  NS  ${ns}." >> config/secondary.ns
    done

    # add ufw rules for allowing axfr servers
    for server_ip in $AXFR_SERVERS; do
        cs exec \
           ufw allow from $server_ip to any port 53    
    done
    cs exec ufw enable

}
